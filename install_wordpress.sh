#!bin/bash

sudo yum update -y
sudo yum install -y docker
sudo usermod -a -G docker ec2-user
sudo service docker start
sudo yum install -y python3 python-pip3
sudo pip3 install docker-compose
docker pull wordpress
docker pull mysql
docker run --name mysql_take_home -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password -d mysql
docker run --name wordpress_take_home --link mysql_take_home -p 80:80 -d wordpress
docker run --restart=yes