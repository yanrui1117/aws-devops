variable "aws_access_key" {
  default = "AWS_ACCESS_KEY"
}
variable "aws_secret_key" {
  default = "AWS_SECRET_KEY"
}
variable "aws_key_path" {
  default = ""
}
variable "aws_key_name" {
  default = "TakeHomeAssignment"
}
variable "aws_region" {
  description = "EC2 Region foe VPC"
  default     = "ap-southeast-1a"
}

variable "amis" {
  description = "AMIs by region"
  default = {
    ap-southeast-1 = "ami-05c64f7b4062b0a21" # Amazon Linux 2
  }
}

variable "any_public" {
  description = "CIDR for all public entry"
  default     = "0.0.0.0/0"
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet1_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "10.0.0.0/24"
}

variable "private_subnet1_cidr" {
  description = "CIDR for the Private Subnet"
  default     = "10.0.4.0/24"
}

variable "my_ip_address" {
  description = "my ip address for ssh"
  default     = "182.55.114.73/32"
}

variable "all_subnets" {
  description = "All three subnets in this AZ"
  default     = ["subnet-60fb4e39", "subnet-7345aa3b", "subnet-c4f806a2"]
}

variable "public_subnet2_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "10.0.2.0/24"
}


variable "asg_min" {
  default     = "1"
}

variable "asg_max" {
  default     = "5"
}

variable "asg_desired" {
  default     = "1"
}

variable "available_zones"{
  default     = ["ap-southeast_1a","ap-southeast_1b","ap-southeast_1c"]
}

variable "public_subnet3_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "10.0.3.0/24"
}

variable "ec2_count" {
  description = "For easy Scaling, the Number of web server instances"
  type        = number
  default     = 3
}
