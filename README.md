# Infrastructure as Code

A Mini Project to write code to provision infrastructure on AWS cloud.

## Getting Started
Use AWS cloud as the platform, with Terraform as the main tool, firstly understand deeply on the concepts of each AWS cloud terms, 

- `VPC`
  - `Router`
  - `Internet gateway`
  - `Routing table`
  - `Subnet`
  - `Security group` 
  - `VM instance` 
- `RDS`
- `ElasticCache`
- `Security groups`
- `Load balancer` 
- `Target Groups` 
- `Auto Scaling Groups` 
- `Elastic IP` 

## Prerequisites

Manually setup an EC2 instance, hands on configurations on each term above, also login to that created VM, for docker, mysql, wordpress and nginx setups.

## Installing

Installed Terraform, apply account for AWS, docker, mysql, wordpress installation on VM.

## Procedures

After learning and comprehending related knowledge, start to code in Terraform, slowly from instance and security group, then added on others bit by bit. Keep testing while developing. Version control by always backup fully working latest code.

## Included on code:

- Custom VPC
- Load balancer related items
- Security groups for instances, load balancer
- EIP
- Public and Private Subnets
- Dynamic Block for easy Scaling -- configurable on variables file
- Has tried RDS, but failed to get the aws_rds_instance_cluster, therefore finally choose to start mysql docker image for DB.

## Deployment

Deployed around 50 times during three days working time.

## Author
- Rui Yan 
	- Linkedin: https://www.linkedin.com/in/rui-yan-8b3293bb/
 

