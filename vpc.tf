locals {
  vm_ids = {
    for i in range(var.ec2_count) : "${i}" => format("vm-%02d", i)
  }
}


resource "aws_vpc" "example" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.example.id}"
}


//ELB

resource "aws_security_group" "lb_sg" {
    name = "lb_sg"
    description = "security group for Application load balancer."
    vpc_id = "${aws_vpc.example.id}"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_lb" "alb" {
  name               = "lb-tf"
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb_sg.id}"]
  subnets            = ["${aws_subnet.ap-southeast-1a-public.id}","${aws_subnet.ap-southeast-1b-public.id}","${aws_subnet.ap-southeast-1c-public.id}"]
}


resource "aws_lb_target_group" "alb" {
  name     = "tf-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.example.id}"
  target_type = "instance"
  health_check {  //## In order to fix the problem of load balancer always showing unhealthy
    enabled = true
    matcher = "302,200" 
  }
}

resource "aws_lb_target_group_attachment" "alb" {
  for_each = local.vm_ids
  target_group_arn = "${aws_lb_target_group.alb.arn}"
  target_id        = "${aws_instance.nat[each.key].id}"
  port             = 80
}

resource "aws_lb_listener" "alb" {
  load_balancer_arn = "${aws_lb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.alb.arn}"
  }
}

//EC2

resource "aws_security_group" "nat" {
  name        = "take-home-vpc_nat"
  description = "SG to allow traffic to pass from the private subnet to the internet"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.lb_sg.id}"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.my_ip_address}"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress { // MySQL
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = "${aws_vpc.example.id}"

}

resource "aws_instance" "nat" {
  for_each = local.vm_ids
  ami                         = "ami-05c64f7b4062b0a21" # hard-coded
  availability_zone           = "${var.aws_region}"
  instance_type               = "t2.micro"
  key_name                    = "${var.aws_key_name}"
  vpc_security_group_ids      = ["${aws_security_group.nat.id}"]
  user_data                   = "${file("install_wordpress.sh")}"
  subnet_id                   = "${aws_subnet.ap-southeast-1a-public.id}"
  associate_public_ip_address = true
  source_dest_check           = false

}

//EIP
resource "aws_eip" "nat" {
  for_each = local.vm_ids
  instance = "${aws_instance.nat[each.key].id}"
  vpc      = true
}


//Public Subnet 1

resource "aws_subnet" "ap-southeast-1a-public" {
  vpc_id = "${aws_vpc.example.id}"

  cidr_block        = "${var.public_subnet1_cidr}"
  availability_zone = "ap-southeast-1a"
}

resource "aws_route_table" "ap-southeast-1a-public" {
  vpc_id = "${aws_vpc.example.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
}

resource "aws_route_table_association" "ap-southeast-1a-public" {
  subnet_id      = "${aws_subnet.ap-southeast-1a-public.id}"
  route_table_id = "${aws_route_table.ap-southeast-1a-public.id}"
}

//Public Subnet 2

resource "aws_subnet" "ap-southeast-1b-public" {
  vpc_id = "${aws_vpc.example.id}"

  cidr_block        = "${var.public_subnet2_cidr}"
  availability_zone = "ap-southeast-1b"
}

resource "aws_route_table" "ap-southeast-1b-public" {
  vpc_id = "${aws_vpc.example.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
}

resource "aws_route_table_association" "ap-southeast-1b-public" {
  subnet_id      = "${aws_subnet.ap-southeast-1b-public.id}"
  route_table_id = "${aws_route_table.ap-southeast-1b-public.id}"
}

//Public Subnet 3

resource "aws_subnet" "ap-southeast-1c-public" {
  vpc_id = "${aws_vpc.example.id}"

  cidr_block        = "${var.public_subnet3_cidr}"
  availability_zone = "ap-southeast-1c"
}

resource "aws_route_table" "ap-southeast-1c-public" {
  vpc_id = "${aws_vpc.example.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
}

resource "aws_route_table_association" "ap-southeast-1c-public" {
  subnet_id      = "${aws_subnet.ap-southeast-1c-public.id}"
  route_table_id = "${aws_route_table.ap-southeast-1c-public.id}"
}



//Private Subnet 1

resource "aws_subnet" "ap-southeast-1a-private" {
  vpc_id = "${aws_vpc.example.id}"

  cidr_block        = "${var.private_subnet1_cidr}"
  availability_zone = "ap-southeast-1a"
}

resource "aws_route_table" "ap-southeast-1a-private" {
  vpc_id = "${aws_vpc.example.id}"

}

resource "aws_route_table_association" "ap-southeast-1a-private" {
  for_each = local.vm_ids
  subnet_id      = "${aws_subnet.ap-southeast-1a-private.id}"
  route_table_id = "${aws_route_table.ap-southeast-1a-private.id}"
}


